from django.db import models
from django.utils.translation import gettext_lazy as _


class State(models.TextChoices):
    ACTIVE = "active", _("ACTIVE")
    ARCHIVED = "archived", _("ARCHIVED")


class BaseModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    updated_at = models.DateTimeField(auto_now=True)
    state = models.CharField(
        max_length=8,
        choices=State.choices,
        default=State.ACTIVE,
        db_default=State.ACTIVE,  # type: ignore
        blank=True,
    )

    class Meta:
        abstract = True


def dictfetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return [dict(zip(columns, row)) for row in cursor.fetchall()]
