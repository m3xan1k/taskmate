from django.http.response import HttpResponseRedirect


class HttpResponseSeeOther(HttpResponseRedirect):
    status_code = 303
