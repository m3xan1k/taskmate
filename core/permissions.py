from django.contrib.auth.mixins import AccessMixin
from django.shortcuts import redirect, get_object_or_404


class OwnerMixin(AccessMixin):

    def dispatch(self, request, *args, **kwargs):
        user = self.request.user  # type: ignore
        obj = get_object_or_404(self.model, pk=kwargs.get("pk"))  # type: ignore

        if user == obj.user or user.is_superuser:
            return super().dispatch(request, *args, **kwargs)  # type: ignore
        self.handle_no_permission()
