from django.urls import path, include
from django.conf import settings

from tasks import views as tasks_views


urlpatterns = [
    path("", tasks_views.TaskListView.as_view(), name="task-list"),
    path("tasks/", tasks_views.TaskListView.as_view(), name="task-list"),
    path("tasks/agenda/", tasks_views.task_agenda, name="task-agenda"),
    path("tasks/<int:pk>/", tasks_views.TaskDetailView.as_view(), name="task-detail"),  # noqa
    path("tasks/create/", tasks_views.TaskCreateView.as_view(), name="task-create"),  # noqa
    path("tasks/<int:pk>/update/", tasks_views.TaskUpdateView.as_view(), name="task-update"),  # noqa
    path("tasks/<int:pk>/delete/", tasks_views.TaskDeleteView.as_view(), name="task-delete"),  # noqa
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        path("__debug__/", include(debug_toolbar.urls))  # type: ignore
    ] + urlpatterns

DEBUG_TOOLBAR_CONFIG = {
    "SHOW_TOOLBAR_CALLBACK": lambda request: True,
}
