from datetime import timedelta
from django.db import connection

from django.db.models.query import RawQuerySet
from django.utils import timezone

from tasks.models import Task
from core.models import dictfetchall


def update_task(
    instance: "Task",
    cleaned_data: dict,
    fields_to_update: set,
    partial: bool,
) -> "Task":
    data = cleaned_data
    if partial:
        data = {k: cleaned_data[k] for k in fields_to_update if k in cleaned_data}

    for field in data:
        setattr(instance, field, data[field])

    instance.save()
    return instance


def collect_daily(user_id: int) -> 'RawQuerySet[Task]':

    tasks = Task.objects.raw(
        """
        select id, name, priority, scheduled_at, deadline_at
        from tasks_task
        where state = 'active'
            and status in ('todo', 'next')
            and scheduled_at <= now()::date
            and user_id = %s
        """,
        (user_id, )
    )
    return tasks


def collect_weekly_agenda(user_id: int) -> list[dict]:

    today = timezone.now().date()

    start_of_week = today - timedelta(days=today.weekday())

    dates = [(start_of_week + timedelta(days=i)).strftime("%Y-%m-%d") for i in range(7)]
    days = [(start_of_week + timedelta(days=i)).strftime("%A") for i in range(7)]

    select_statements = " UNION ALL ".join(
        [f"SELECT '{day}' as day, '{date}' as date" for day, date in zip(days, dates)]
    )

    query = f"""
        SELECT d.*, t.*
        FROM ({select_statements}) as d
        LEFT JOIN tasks_task t
        ON d.date::date = t.scheduled_at
            and t.user_id = %s
        ORDER BY d.date, t.priority;
    """

    with connection.cursor() as cursor:
        cursor.execute(query, (user_id, ))
        rows = dictfetchall(cursor)

    return rows
