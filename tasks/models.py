from django.contrib.auth.models import User
from django.db import models
from django.db.models import CheckConstraint, Q
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from core.models import BaseModel, State


class TaskRepeatType(models.TextChoices):
    FIX = "fix", _("FIX")
    FLEX = "flex", _("FLEX")


class TaskStatus(models.TextChoices):
    TODO = "todo", _("TODO")
    NEXT = "next", _("NEXT")
    DONE = "done", _("DONE")


class TaskDueStatus(models.TextChoices):
    OK = "ok", _("OK")
    SOON = "soon", _("SOON")
    OVERDUE = "overdue", _("OVERDUE")


class TaskPriority(models.TextChoices):
    LOW = "low", _("LOW")
    MID = "mid", _("MID")
    HIGH = "high", _("HIGH")


class Task(BaseModel):
    name = models.CharField(
        max_length=128,
        unique=True,
        db_index=True,
    )
    description = models.TextField(
        blank=True,
        default="",
        db_default="",  # type: ignore
    )
    scheduled_at = models.DateField()
    deadline_at = models.DateField(null=True)
    repeat = models.BooleanField(default=False, db_default=False)  # type: ignore
    repeat_type = models.CharField(
        max_length=16,
        choices=TaskRepeatType.choices,
        null=True,
    )
    repeat_interval = models.DurationField(null=True)
    status = models.CharField(
        max_length=16,
        choices=TaskStatus.choices,
        default=TaskStatus.TODO,
        db_default=TaskStatus.TODO,  # type: ignore
    )
    priority = models.CharField(
        max_length=4,
        choices=TaskPriority.choices,
        default=TaskPriority.LOW,
        db_default=TaskPriority.LOW,  # type: ignore
    )

    user = models.ForeignKey(
        User,
        related_name='tasks',
        on_delete=models.CASCADE,
    )

    class Meta:
        constraints = [
            CheckConstraint(
                check=Q(
                    repeat_type__in=[choice[0] for choice in TaskRepeatType.choices]
                ),
                name="task_repeat_type_valid",
            ),
            CheckConstraint(
                check=Q(
                    status__in=[choice[0] for choice in TaskStatus.choices],
                ),
                name="task_status_valid",
            ),
            CheckConstraint(
                check=Q(
                    priority__in=[choice[0] for choice in TaskPriority.choices],
                ),
                name="task_priority_valid",
            ),
        ]

    def save(self, *args, **kwargs):
        if all(
            [
                self.pk is not None,
                self.state == State.ACTIVE.value,
                self.status == TaskStatus.DONE.value,
                self.repeat,
            ]
        ):
            original = Task.objects.get(pk=self.pk)

            delta = None
            has_deadline_unchanged = (
                self.deadline_at is not None
                and self.deadline_at == original.deadline_at  # noqa
            )

            if has_deadline_unchanged:
                delta = self.deadline_at - self.scheduled_at

            if self.repeat_type == TaskRepeatType.FIX.value:
                self.scheduled_at += self.repeat_interval

            elif self.repeat_type == TaskRepeatType.FLEX.value:
                self.scheduled_at = timezone.now().date() + self.repeat_interval

            if has_deadline_unchanged:
                self.deadline_at = self.scheduled_at + delta

        return super().save(*args, **kwargs)
