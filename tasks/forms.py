from datetime import timedelta
from django import forms
from tasks.models import Task, TaskRepeatType, TaskStatus, TaskPriority


class TaskForm(forms.ModelForm):
    name = forms.CharField(max_length=128, widget=forms.TextInput())
    description = forms.CharField(widget=forms.Textarea(), required=False)
    scheduled_at = forms.DateField(
        widget=forms.DateInput(
            format="%Y-%m-%d",
            attrs={"type": "date"},
        ),
    )
    deadline_at = forms.DateField(
        required=False,
        widget=forms.DateInput(
            attrs={
                "class": "form-control",
                "type": "date",
            }
        ),
    )
    repeat = forms.BooleanField(required=False, widget=forms.CheckboxInput())
    repeat_type = forms.ChoiceField(
        choices=TaskRepeatType.choices,
        required=False,
        widget=forms.Select(),
    )
    repeat_interval = forms.DurationField(
        required=False,
        widget=forms.NumberInput(),
    )
    status = forms.ChoiceField(
        choices=TaskStatus.choices,
        required=False,
        widget=forms.Select(),
    )
    priority = forms.ChoiceField(
        choices=TaskPriority.choices,
        required=False,
        widget=forms.Select(),
    )

    class Meta:
        model = Task
        fields = [
            "name",
            "description",
            "scheduled_at",
            "deadline_at",
            "repeat",
            "repeat_type",
            "repeat_interval",
            "status",
            "priority",
        ]

    def __init__(self, *args, partial=False, user=None, **kwargs):
        super().__init__(*args, **kwargs)
        self.partial = partial
        self.user = user

        if partial:
            for field in self.fields:
                if field not in self.data:
                    self.fields[field].required = False

        if not all([self.instance, self.instance.pk]):
            self.fields.pop("status")

    def clean(self):
        cleaned_data: dict = super().clean()  # type: ignore
        repeat = cleaned_data.get("repeat")
        repeat_type = cleaned_data.get("repeat_type")
        repeat_interval = cleaned_data.get("repeat_interval")
        scheduled_at = cleaned_data.get("scheduled_at")
        deadline_at = cleaned_data.get("deadline_at")

        if repeat and not repeat_type:
            raise forms.ValidationError(
                {
                    "repeat_type": [
                        "If a task is set to repeat, a repeat type must be selected."
                    ]
                }
            )
        if repeat and not repeat_interval:
            raise forms.ValidationError(
                {
                    "repeat_interval": [
                        "If a task is set to repeat, a repeat interval must be specified."  # noqa
                    ]
                }
            )
        if not repeat:
            cleaned_data["repeat_type"] = None
            cleaned_data["repeat_interval"] = None

        if all([scheduled_at, deadline_at]):
            if deadline_at <= scheduled_at:  # type: ignore
                raise forms.ValidationError(
                    {"deadline_at": ["Task deadline cannot be earlier than scheduled."]}
                )

        self.instance.user = self.user

        return cleaned_data

    def clean_repeat_interval(self):
        data = self.cleaned_data.get("repeat_interval")
        if data is not None:
            return timedelta(days=data.seconds)
