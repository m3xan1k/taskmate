import os

from django.core.management.base import BaseCommand
from django.core.mail import send_mail
from django.utils import timezone
from django.urls import reverse

from tasks.services import collect_daily


BASE_URL = os.environ["BASE_URL"]
FROM_EMAIL = os.environ["EMAIL_HOST_USER"]
TO_EMAIL = os.environ["NOTIFICATION_EMAIL"]


class Command(BaseCommand):

    help = "Sends email with scheduled undone tasks"

    def handle(self, *args, **options):

        undone_tasks = collect_daily()

        subject = "Tasks on {}".format(
            timezone.now().date().strftime("%Y-%m-%d")
        )

        message = ""

        for task in undone_tasks:
            scheduled_at = task.scheduled_at.strftime("%Y-%m-%d")
            deadline_at = ""
            if task.deadline_at is not None:
                task.deadline_at.strftime("%Y-%m-%d")
            message += "{}\t{}\t{}\t{}\t{}\n".format(
                task.name,
                task.priority,
                scheduled_at,
                deadline_at,
                BASE_URL + reverse('task-detail', kwargs={"pk": task.id}),  # type: ignore
            )

        send_mail(
            subject=subject,
            message=message,
            from_email=FROM_EMAIL,
            recipient_list=[TO_EMAIL],
            fail_silently=False,
        )
