from django.views import generic, View
from django.http.request import HttpRequest
from django.http import QueryDict
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.db.models import QuerySet
from django.contrib.auth.mixins import LoginRequiredMixin

from tasks.models import Task
from tasks.forms import TaskForm
from tasks.services import update_task, collect_weekly_agenda
from core.http import HttpResponseSeeOther
from core.permissions import OwnerMixin


class TaskListView(LoginRequiredMixin, generic.ListView):
    context_object_name = "tasks"
    queryset = Task.objects.filter(state="active")
    template_name = "tasks/list.html"

    def get_queryset(self):
        qs: QuerySet = super().get_queryset()  # type: ignore

        if not self.request.user.is_superuser:  # type: ignore
            qs = qs.filter(user=self.request.user)

        return qs


class TaskDetailView(LoginRequiredMixin, OwnerMixin, generic.DetailView):
    model = Task
    context_object_name = "task"
    template_name = "tasks/detail.html"


class TaskCreateView(LoginRequiredMixin, generic.CreateView):
    model = Task
    form_class = TaskForm
    template_name = "tasks/form.html"

    def form_valid(self, form):
        super().form_valid(form)
        return HttpResponseSeeOther(reverse("task-list"))

    def get_success_url(self) -> str:
        return reverse("task-list")

    def get_form_kwargs(self) -> dict:
        kwargs = super().get_form_kwargs()
        kwargs['user'] = self.request.user
        return kwargs


class TaskUpdateView(LoginRequiredMixin, OwnerMixin, View):
    model = Task
    form_class = TaskForm
    template_name = "tasks/form.html"

    def _update(self, request: HttpRequest, pk: int, partial: bool):
        request_data = QueryDict(request.body)  # type: ignore
        task = get_object_or_404(self.model, pk=pk)
        form = self.form_class(
            request_data,
            user=self.request.user,
            instance=task,
            partial=partial,
        )

        if form.is_valid():
            task.refresh_from_db()
            update_task(
                instance=task,
                cleaned_data=form.cleaned_data,
                fields_to_update=set(request_data),
                partial=partial,
            )
            return HttpResponseSeeOther(reverse("task-list"))
        return render(request, self.template_name, {"form": form})

    def put(self, request: HttpRequest, pk: int):
        return self._update(request, pk, False)

    def patch(self, request: HttpRequest, pk: int):
        return self._update(request, pk, True)

    def get(self, request: HttpRequest, pk: int):
        task = get_object_or_404(self.model, pk=pk)
        form = self.form_class(instance=task, partial=False)
        return render(request, self.template_name, {"form": form})


class TaskDeleteView(LoginRequiredMixin, OwnerMixin, View):
    model = Task

    def delete(self, request: HttpRequest, pk: int):
        task = get_object_or_404(Task, pk=pk)
        task.delete()
        return HttpResponseSeeOther(reverse("task-list"))


def task_agenda(LoginRequiredMixin, request: HttpRequest):
    rows = collect_weekly_agenda(request.user.pk)
    return render(request, 'tasks/agenda.html', {'rows': rows})
