CREATE TRIGGER update_status_after_done
AFTER UPDATE OF status ON tasks_task
FOR EACH ROW
WHEN NEW.status = 'done' AND OLD.status != 'done' AND NEW.repeat = 1 AND NEW.state != 'archived'
BEGIN
 -- Update the status to 'todo' if scheduled_at is more than now, otherwise set it to 'next'
 UPDATE tasks_task
 SET status =
    CASE WHEN NEW.scheduled_at > datetime('now') THEN 'todo' ELSE 'next' END
 WHERE id = NEW.id;
END;

