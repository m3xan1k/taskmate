from copy import deepcopy
from datetime import timedelta
from urllib.parse import urlencode
import uuid

from django.test import TestCase
from django.urls import reverse
from django.utils import timezone
from django.contrib.auth import get_user_model

from tasks.forms import TaskForm
from tasks.models import TaskPriority, TaskRepeatType, TaskStatus, Task
from tasks.services import collect_daily, collect_weekly_agenda
from core.models import State
from tests.common import UserTestCase


TASK_DATA = {
    "name": "Test task",
    "description": "This is a test task.",
    "scheduled_at": timezone.now().date() + timedelta(days=1),
    "deadline_at": timezone.now().date() + timedelta(days=2),
    "repeat": True,
    "repeat_type": TaskRepeatType.FIX,
    "repeat_interval": 3,
    "status": TaskStatus.TODO,
    "priority": TaskPriority.LOW,
}


class TaskFormTest(UserTestCase):

    def setUp(self):
        super().setUp()
        self.client.login(
            username='test',
            password='Admin12345',
        )

    def test_minimal_success(self):
        test_fields = ["name", "scheduled_at"]
        form = TaskForm(
            data={f: TASK_DATA[f] for f in test_fields},
            user=self.user,
        )
        self.assertTrue(form.is_valid())
        form.save()

        task: Task = Task.objects.first()  # type: ignore
        self.assertIsNotNone(task)
        self.assertEqual(task.name, TASK_DATA["name"])
        self.assertEqual(task.scheduled_at, TASK_DATA["scheduled_at"])

    def test_empty(self):
        form = TaskForm(data={}, user=self.user)
        self.assertFalse(form.is_valid())
        self.assertEqual(len(form.errors), 2)
        self.assertIn("name", form.errors)
        self.assertIn("scheduled_at", form.errors)

    def test_no_repeat_clean_fields(self):
        data = deepcopy(TASK_DATA)
        data["repeat"] = False
        form = TaskForm(data=data, user=self.user)
        self.assertTrue(form.is_valid())
        form.save()

        task: Task = Task.objects.first()  # type: ignore
        self.assertIsNotNone(task)
        self.assertFalse(task.repeat)
        self.assertIsNone(task.repeat_type)
        self.assertIsNone(task.repeat_interval)

    def test_interval_is_days(self):
        form = TaskForm(data=TASK_DATA, user=self.user)
        self.assertTrue(form.is_valid())
        form.save()

        task: Task = Task.objects.first()  # type: ignore
        self.assertIsNotNone(task)
        self.assertTrue(task.repeat)
        self.assertEqual(task.repeat_type, TaskRepeatType.FIX.value)
        self.assertEqual(
            task.repeat_interval,
            timedelta(days=TASK_DATA["repeat_interval"]),
        )

    def test_repeat_done_fix(self):
        form = TaskForm(data=TASK_DATA, user=self.user)
        self.assertTrue(form.is_valid())
        form.save()

        task: Task = Task.objects.first()  # type: ignore
        self.assertEqual(task.status, TaskStatus.TODO.value)
        self.assertEqual(task.scheduled_at, TASK_DATA["scheduled_at"])
        self.assertEqual(task.deadline_at, TASK_DATA["deadline_at"])

        task.status = TaskStatus.DONE.value
        task.save()

        task.refresh_from_db()
        self.assertEqual(task.status, TaskStatus.TODO.value)
        self.assertEqual(
            task.scheduled_at,
            TASK_DATA["scheduled_at"] + task.repeat_interval,
        )
        self.assertEqual(
            task.deadline_at,
            TASK_DATA["deadline_at"] + task.repeat_interval,
        )

    def test_repeat_done_flex(self):
        data = deepcopy(TASK_DATA)
        data["repeat_type"] = TaskRepeatType.FLEX.value
        form = TaskForm(data=data, user=self.user)
        self.assertTrue(form.is_valid())
        form.save()

        task: Task = Task.objects.first()  # type: ignore

        self.assertEqual(task.status, TaskStatus.TODO.value)
        self.assertEqual(task.scheduled_at, TASK_DATA["scheduled_at"])
        self.assertEqual(task.deadline_at, TASK_DATA["deadline_at"])

        delta = task.deadline_at - task.scheduled_at

        task.status = TaskStatus.DONE.value
        task.save()

        task.refresh_from_db()
        self.assertEqual(task.status, TaskStatus.TODO.value)
        self.assertEqual(
            task.scheduled_at,
            timezone.now().date() + task.repeat_interval,
        )
        self.assertEqual(
            task.deadline_at,
            task.scheduled_at + delta,
        )

    def test_archive_not_affected(self):
        form = TaskForm(data=TASK_DATA, user=self.user)
        self.assertTrue(form.is_valid())
        form.save()

        task: Task = Task.objects.first()  # type: ignore
        self.assertEqual(task.status, TaskStatus.TODO.value)
        self.assertEqual(task.scheduled_at, TASK_DATA["scheduled_at"])
        self.assertEqual(task.deadline_at, TASK_DATA["deadline_at"])

        task.state = State.ARCHIVED.value
        task.save()

        task.status = TaskStatus.DONE.value
        task.save()

        task.refresh_from_db()
        self.assertEqual(task.status, TaskStatus.DONE.value)
        self.assertEqual(task.scheduled_at, TASK_DATA["scheduled_at"])
        self.assertEqual(task.deadline_at, TASK_DATA["deadline_at"])

    def test_not_repeat_not_affected(self):
        data = deepcopy(TASK_DATA)
        data["repeat"] = False
        form = TaskForm(data=data, user=self.user)
        self.assertTrue(form.is_valid())
        form.save()

        task: Task = Task.objects.first()  # type: ignore
        self.assertEqual(task.status, TaskStatus.TODO.value)
        self.assertEqual(task.scheduled_at, TASK_DATA["scheduled_at"])
        self.assertEqual(task.deadline_at, TASK_DATA["deadline_at"])

        task.status = TaskStatus.DONE.value
        task.save()

        task.refresh_from_db()
        self.assertEqual(task.status, TaskStatus.DONE.value)
        self.assertEqual(task.scheduled_at, TASK_DATA["scheduled_at"])
        self.assertEqual(task.deadline_at, TASK_DATA["deadline_at"])

    def test_no_repeat_fields(self):
        form = TaskForm(
            data={f: TASK_DATA[f] for f in TASK_DATA if not f == "repeat_type"},
            user=self.user,
        )
        self.assertFalse(form.is_valid())
        self.assertIn("repeat_type", form.errors)

        form = TaskForm(
            data={f: TASK_DATA[f] for f in TASK_DATA if not f == "repeat_interval"},
            user=self.user,
        )
        self.assertFalse(form.is_valid())
        self.assertIn("repeat_interval", form.errors)

    def test_scheduled_later_than_deadline(self):
        data = deepcopy(TASK_DATA)
        data["scheduled_at"] = timezone.now().date() + timedelta(days=2)
        data["deadline_at"] = timezone.now().date() + timedelta(days=1)
        form = TaskForm(data=data, user=self.user)
        self.assertFalse(form.is_valid())
        self.assertIn("deadline_at", form.errors)


class TaskViewTest(UserTestCase):

    def setUp(self):
        super().setUp()
        self.client.login(
            username='test',
            password='Admin12345',
        )

    def test_crud(self):
        res = self.client.get(reverse("task-create"))
        self.assertEqual(res.status_code, 200)
        self.assertTemplateUsed(res, "tasks/form.html")

        # create
        res = self.client.post(
            reverse("task-create"),
            TASK_DATA,
            follow=True,
        )

        task: Task = Task.objects.first()  # type: ignore
        self.assertIsNotNone(task)
        self.assertRedirects(
            res,
            reverse("task-list"),
            status_code=303,
            target_status_code=200,
            fetch_redirect_response=True,
        )

        # put
        task_data = deepcopy(TASK_DATA)
        task_data["name"] = "Updated task"
        task_data_encoded = urlencode(task_data)
        res = self.client.put(
            reverse("task-update", kwargs={"pk": task.pk}),
            data=task_data_encoded,
            content_type="application/x-www-form-urlencoded",
        )

        task.refresh_from_db()
        self.assertEqual(task.name, "Updated task")
        self.assertRedirects(
            res,
            reverse("task-list"),
            status_code=303,
            target_status_code=200,
            fetch_redirect_response=True,
        )

        # patch
        res = self.client.patch(
            reverse("task-update", kwargs={"pk": task.pk}),
            data=urlencode({"name": "Patched task", "priority": "high"}),
            content_type="application/x-www-form-urlencoded",
        )

        task.refresh_from_db()
        self.assertEqual(task.name, "Patched task")
        self.assertEqual(task.priority, "high")
        self.assertRedirects(
            res,
            reverse("task-list"),
            status_code=303,
            target_status_code=200,
            fetch_redirect_response=True,
        )

        # retrieve
        res = self.client.get(reverse("task-detail", kwargs={"pk": task.pk}))
        self.assertEqual(res.status_code, 200)
        self.assertTemplateUsed("tasks/detail.html")

        # list
        res = self.client.get(reverse("task-list"))
        self.assertEqual(res.status_code, 200)
        self.assertTemplateUsed("tasks/list.html")

        # delete
        res = self.client.delete(reverse("task-delete", kwargs={"pk": task.pk}))
        self.assertFalse(Task.objects.filter(pk=task.pk).exists())
        self.assertEqual(Task.objects.count(), 0)


class TaskServiceTest(UserTestCase):

    def create_task(self, **overrides):
        data = deepcopy(TASK_DATA)
        data["user"] = self.user

        data['name'] += " " + str(uuid.uuid4())

        if overrides:
            data = {**data, **overrides}

        if data.get('repeat_interval'):
            data['repeat_interval'] = timedelta(days=data['repeat_interval'])

        return Task.objects.create(**data)

    def test_collect_daily(self):
        task_1 = self.create_task()
        task_2 = self.create_task(
            scheduled_at=timezone.now().date() - timedelta(days=1)
        )
        task_3 = self.create_task(
            scheduled_at=timezone.now().date()
        )

        self.assertEqual(Task.objects.count(), 3)

        collected_tasks = collect_daily(self.user.pk)
        self.assertEqual(len(collected_tasks), 2)
        self.assertNotIn(task_1, collected_tasks)
        self.assertIn(task_2, collected_tasks)
        self.assertIn(task_3, collected_tasks)

        task_3.repeat = False
        task_3.status = TaskStatus.DONE.value
        task_3.save()

        collected_tasks = collect_daily(self.user.pk)
        self.assertEqual(len(collected_tasks), 1)
        self.assertIn(task_2, collected_tasks)

    def test_weekly_agenda(self):
        today = timezone.now().date()
        start_of_week = today - timedelta(days=today.weekday())

        task_1 = self.create_task(
            scheduled_at=start_of_week
        )
        task_2 = self.create_task(
            scheduled_at=start_of_week + timedelta(days=1)
        )
        task_3 = self.create_task(
            scheduled_at=start_of_week + timedelta(days=2)
        )
        task_4 = self.create_task(
            scheduled_at=start_of_week + timedelta(days=2)
        )

        self.assertEqual(Task.objects.count(), 4)

        rows = collect_weekly_agenda(self.user.pk)
        self.assertEqual(len(rows), 8)


class TestUserIntegration(TestCase):

    def setUp(self):
        super().setUp()
        self.user_1 = get_user_model().objects.create_user(  # type: ignore
            username='test',
            password='Admin12345',
        )
        self.user_2 = get_user_model().objects.create_user(  # type: ignore
            username='test2',
            password='Admin12345',
        )
        self.user_1_task_1 = self.create_task(self.user_1)
        self.user_1_task_2 = self.create_task(self.user_1)
        self.user_2_task_1 = self.create_task(self.user_2)
        self.user_2_task_2 = self.create_task(self.user_2)

    def create_task(self, user, **overrides):
        data = deepcopy(TASK_DATA)
        data["user"] = user

        data['name'] += " " + str(uuid.uuid4())

        if overrides:
            data = {**data, **overrides}

        if data.get('repeat_interval'):
            data['repeat_interval'] = timedelta(days=data['repeat_interval'])

        return Task.objects.create(**data)

    def test_list_corresponds_user(self):
        self.client.login(
            username='test',
            password='Admin12345',
        )
        res = self.client.get(reverse('task-list'))
        tasks = res.context['tasks']
        self.assertEqual(len(tasks), 2)
        self.assertIn(self.user_1_task_1, tasks)
        self.assertIn(self.user_1_task_2, tasks)

        self.client.login(
            username='test2',
            password='Admin12345',
        )
        res = self.client.get(reverse('task-list'))
        tasks = res.context['tasks']
        self.assertEqual(len(tasks), 2)
        self.assertIn(self.user_2_task_1, tasks)
        self.assertIn(self.user_2_task_2, tasks)

    def test_crud_corresponds_user(self):
        self.client.login(
            username='test',
            password='Admin12345',
        )

        # detail
        res = self.client.get(reverse(
            'task-detail', kwargs={'pk': self.user_1_task_1.pk}
        ))
        self.assertEqual(res.status_code, 200)

        res = self.client.get(reverse(
            'task-detail', kwargs={'pk': self.user_2_task_1.pk}
        ))
        self.assertEqual(res.status_code, 403)

        # patch
        res = self.client.patch(
            reverse('task-update', kwargs={'pk': self.user_1_task_1.pk}),
            data=urlencode({"name": "Patched task"}),
            content_type="application/x-www-form-urlencoded",
        )
        self.assertEqual(res.status_code, 303)
        self.user_1_task_1.refresh_from_db()
        self.assertEqual(self.user_1_task_1.name, "Patched task")

        old_name = self.user_2_task_1.name
        res = self.client.patch(
            reverse('task-update', kwargs={'pk': self.user_2_task_1.pk}),
            data=urlencode({"name": "Patched task"}),
            content_type="application/x-www-form-urlencoded",
        )
        self.assertEqual(res.status_code, 403)
        self.user_2_task_1.refresh_from_db()
        self.assertEqual(self.user_2_task_1.name, old_name)

        # delete
        res = self.client.delete(
            reverse('task-delete', kwargs={'pk': self.user_1_task_1.pk}),
        )
        self.assertEqual(res.status_code, 303)
        self.assertIsNone(Task.objects.filter(pk=self.user_1_task_1.pk).first())

        res = self.client.delete(
            reverse('task-delete', kwargs={'pk': self.user_2_task_1.pk}),
        )
        self.assertEqual(res.status_code, 403)
        self.assertIsNotNone(Task.objects.filter(pk=self.user_2_task_1.pk).first())

    def test_not_authenticated(self):
        res = self.client.get(reverse('task-list'))
        self.assertEqual(res.status_code, 302)

        res = self.client.get(reverse(
            'task-detail', kwargs={'pk': self.user_1_task_1.pk}
        ))
        self.assertEqual(res.status_code, 302)

        old_name = self.user_1_task_1.name
        res = self.client.patch(
            reverse('task-update', kwargs={'pk': self.user_1_task_1.pk}),
            data=urlencode({"name": "Patched task"}),
            content_type="application/x-www-form-urlencoded",
        )
        self.assertEqual(res.status_code, 302)
        self.user_1_task_1.refresh_from_db()
        self.assertEqual(self.user_1_task_1.name, old_name)

        res = self.client.delete(reverse(
            'task-delete', kwargs={'pk': self.user_1_task_1.pk}
        ))
        self.assertEqual(res.status_code, 302)
        self.assertIsNotNone(
            Task.objects.filter(pk=self.user_1_task_1.pk).first()
        )
