from django.contrib.auth import get_user_model
from django.test import TestCase


class UserTestCase(TestCase):

    def setUp(self):
        super().setUp()
        self.user = get_user_model().objects.create_user(  # type: ignore
            username='test',
            password='Admin12345',
        )
